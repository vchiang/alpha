from django.urls import path
from accounts.views import account_login, user_logout, signup

urlpatterns = [
    path("login/", account_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup"),
]
