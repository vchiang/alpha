# Generated by Django 4.2.7 on 2023-11-07 19:06

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("projects", "0001_initial"),
    ]

    operations = [
        migrations.RenameField(
            model_name="project",
            old_name="owner",
            new_name="owner_id",
        ),
    ]
